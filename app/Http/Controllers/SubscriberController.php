<?php

namespace App\Http\Controllers;

use App\Subscriber;
use Illuminate\Http\Request;
use function redirect;
use function route;

class SubscriberController extends Controller
{
    public function store(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email|unique:subscribers',
        ]);

        $subscriber = new Subscriber();
        $subscriber->email = $request->email;
        $subscriber->save();

        return redirect(route('mainhome'))->with('successMsg', 'Your email added to our subscription list.');

    }
}
