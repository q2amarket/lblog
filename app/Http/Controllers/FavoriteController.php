<?php

namespace App\Http\Controllers;

use App\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use function dd;
use function redirect;

class FavoriteController extends Controller
{
    public function add(Post $post)
    {
        $user       = Auth::user();
        $isFavorite = $user->favorite_posts()->where('post_id', $post->id)->count();

        if ($isFavorite == 0) {
            $user->favorite_posts()->attach($post->id);

            return redirect()->back()->with('successMsg', 'Post added to favorite');
        } else {
            $user->favorite_posts()->detach($post->id);

            return redirect()->back()->with('successMsg', 'Post removed from favorite');
        }
    }
}
