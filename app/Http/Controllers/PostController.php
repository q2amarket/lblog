<?php

namespace App\Http\Controllers;

use App\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use function compact;
use function dd;
use function view;

class PostController extends Controller
{
    public function index()
    {
        $posts = Post::latest()->paginate(9);

        return view('posts', compact('posts'));
    }

    public function details($slug)
    {
        $post = Post::where('slug', $slug)->first();

        $blogKey = 'blog_' . $post->id;

        if ( ! Session::has($blogKey)) {
            $post->increment('view_count');
            Session::put($blogKey, 1);
        }

        $randomPosts = Post::all()->random(3);

        return view('post', compact('post', 'randomPosts'));
    }
}
