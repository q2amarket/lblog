<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use function view;

class DashboardController extends Controller
{
    public function index()
    {
        return view('admin.dashboard');
    }
}
