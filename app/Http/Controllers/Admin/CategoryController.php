<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use function compact;
use function redirect;
use function route;
use function str_slug;
use function uniqid;
use function view;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $categories = Category::latest()->get();

        return view('admin.category.index', compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('admin.category.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name'  => 'required|unique:categories',
            'image' => 'required|mimes:jpeg,png,jpg',
        ]);

        // Get form image
        $image = $request->file('image');
        $slug  = str_slug($request->name);

        if (isset($image)) {

            // make unique name
            $currentdate = Carbon::now()->toDateString();
            $imageName   = $slug . '-' . $currentdate . '-' . uniqid() . '.' . $image->getClientOriginalExtension();

            // check if directory is exists
            if ( ! Storage::disk('public')->exists('category')) {
                Storage::disk('public')->makeDirectory('category');
            }

            // resize image for category
            $category = Image::make($image)->fit(1600, 479)->stream();
            Storage::disk('public')->put('category/' . $imageName, $category);

            // check if directory is exists
            if ( ! Storage::disk('public')->exists('category/slider')) {
                Storage::disk('public')->makeDirectory('category/slider');
            }

            // resize image for category
            $slider = Image::make($image)->fit(500, 333)->stream();
            Storage::disk('public')->put('category/slider/' . $imageName, $slider);

        } else {

            $imageName = 'default.jpg';

        }

        $category        = new Category();
        $category->name  = $request->name;
        $category->slug  = $slug;
        $category->image = $imageName;
        $category->save();

        return redirect(route('admin.category.index'))->with('successMsg', 'Category created successfully!');

    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        $category = Category::find($id);

        return view('admin.category.edit', compact('category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int                      $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name'  => 'required',
            'image' => 'mimes:jpeg,png,jpg',
        ]);

        // Get form image
        $image    = $request->file('image');
        $slug     = str_slug($request->name);
        $category = Category::find($id);

        if (isset($image)) {

            // delete old image from category dir
            if (Storage::disk('public')->exists('category/' . $category->image)) {
                Storage::disk('public')->delete('category/' . $category->image);
            }

            // delete old image from slider dir
            if (Storage::disk('public')->exists('category/slider/' . $category->image)) {
                Storage::disk('public')->delete('category/slider/' . $category->image);
            }

            // make unique name
            $currentdate = Carbon::now()->toDateString();
            $imageName   = $slug . '-' . $currentdate . '-' . uniqid() . '.' . $image->getClientOriginalExtension();

            // check if directory is exists
            if ( ! Storage::disk('public')->exists('category')) {
                Storage::disk('public')->makeDirectory('category');
            }

            // resize image for category
            $categoryImage = Image::make($image)->fit(1600, 479)->stream();
            Storage::disk('public')->put('category/' . $imageName, $categoryImage);

            // check if directory is exists
            if ( ! Storage::disk('public')->exists('category/slider')) {
                Storage::disk('public')->makeDirectory('category/slider');
            }

            // resize image for category
            $sliderImage = Image::make($image)->fit(500, 333)->stream();
            Storage::disk('public')->put('category/slider/' . $imageName, $sliderImage);

        } else {

            $imageName = $category->image;

        }

        $category->name  = $request->name;
        $category->slug  = $slug;
        $category->image = $imageName;
        $category->save();

        return redirect(route('admin.category.index'))->with('successMsg', 'Category updated successfully!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        $category = Category::find($id);

        // delete old image from category dir
        if (Storage::disk('public')->exists('category/' . $category->image)) {
            Storage::disk('public')->delete('category/' . $category->image);
        }

        // delete old image from slider dir
        if (Storage::disk('public')->exists('category/slider/' . $category->image)) {
            Storage::disk('public')->delete('category/slider/' . $category->image);
        }

        $category->delete();

        return redirect(route('admin.category.index'))->with('successMsg', 'Category deleted successfully!');
    }
}
