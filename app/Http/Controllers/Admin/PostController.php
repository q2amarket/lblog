<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\Http\Controllers\Controller;
use App\Notifications\AuthorPostApproved;
use App\Notifications\NewPostNotify;
use App\Post;
use App\Subscriber;
use App\Tag;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use function compact;
use function redirect;
use function route;
use function str_slug;
use function uniqid;
use function view;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $posts = Post::latest()->get();

        return view('admin.post.index', compact('posts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        $categories = Category::all();
        $tags       = Tag::all();

        return view('admin.post.create', compact('categories', 'tags'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title'      => 'required',
            'image'      => 'required|mimes:jpeg,png,jpg',
            'categories' => 'required',
            'tags'       => 'required',
            'body'       => 'required',
        ]);

        // Get form image
        $image = $request->file('image');
        $slug  = str_slug($request->title);

        if (isset($image)) {

            // make unique name
            $currentDate = Carbon::now()->toDateString();
            $imageName   = $slug . '-' . $currentDate . '-' . uniqid() . '.' . $image->getClientOriginalExtension();

            // check if directory is exists
            if ( ! Storage::disk('public')->exists('post')) {
                Storage::disk('public')->makeDirectory('post');
            }

            // resize image for category
            $postImage = Image::make($image)->fit(1600, 1066)->stream();
            Storage::disk('public')->put('post/' . $imageName, $postImage);

        } else {

            $imageName = 'default.jpg';

        }

        $post          = new Post();
        $post->user_id = Auth::id();
        $post->title   = $request->title;
        $post->slug    = $slug;
        $post->image   = $imageName;
        $post->body    = $request->body;
        if (isset($request->status)) {
            $post->status = TRUE;
        } else {
            $post->status = FALSE;
        }
        $post->is_approved = TRUE;
        $post->save();

        $post->categories()->attach($request->categories);
        $post->tags()->attach($request->tags);

        $subscribers = Subscriber::all();

        foreach ($subscribers as $subscriber) {
            Notification::route('mail', $subscriber->email)
                        ->notify(new NewPostNotify($post));
        }

        return redirect(route('admin.post.index'))->with('successMsg', 'Post created successfully!');

    }

    /**
     * Display the specified resource.
     *
     * @param \App\Post $post
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(Post $post)
    {
        return view('admin.post.show', compact('post'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Post $post
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Post $post)
    {
        $categories = Category::all();
        $tags       = Tag::all();

        return view('admin.post.edit', compact('post', 'categories', 'tags'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Post                $post
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, Post $post)
    {
        $this->validate($request, [
            'title'      => 'required',
            'image'      => 'mimes:jpeg,png,jpg',
            'categories' => 'required',
            'tags'       => 'required',
            'body'       => 'required',
        ]);

        // Get form image
        $image = $request->file('image');
        $slug  = str_slug($request->title);

        if (isset($image)) {

            // make unique name
            $currentDate = Carbon::now()->toDateString();
            $imageName   = $slug . '-' . $currentDate . '-' . uniqid() . '.' . $image->getClientOriginalExtension();

            // delete old image
            if (Storage::disk('public')->exists('post/' . $post->image)) {
                Storage::disk('public')->delete('post/' . $post->image);
            }

            // check if directory is exists
            if ( ! Storage::disk('public')->exists('post')) {
                Storage::disk('public')->makeDirectory('post');
            }

            // resize image for category
            $postImage = Image::make($image)->fit(1600, 1066)->stream();
            Storage::disk('public')->put('post/' . $imageName, $postImage);

        } else {

            $imageName = $post->image;

        }

        $post->user_id = Auth::id();
        $post->title   = $request->title;
        $post->slug    = $slug;
        $post->image   = $imageName;
        $post->body    = $request->body;
        if (isset($request->status)) {
            $post->status = TRUE;
        } else {
            $post->status = FALSE;
        }
        $post->is_approved = TRUE;
        $post->save();

        $post->categories()->sync($request->categories);
        $post->tags()->sync($request->tags);

        return redirect(route('admin.post.index'))->with('successMsg', 'Post updated successfully!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Post $post
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Exception
     */
    public function destroy(Post $post)
    {
        // delete old image
        if (Storage::disk('public')->exists('post/' . $post->image)) {
            Storage::disk('public')->delete('post/' . $post->image);
        }

        $post->categories()->detach();
        $post->tags()->detach();
        $post->delete();

        return redirect(route('admin.post.index'))->with('successMsg', 'Post deleted successfully!');
    }

    public function pending()
    {

        $posts = Post::where('is_approved', FALSE)->get();

        return view('admin.post.pending', compact('posts'));

    }

    public function approval(Post $post)
    {
//        $post = Post::find($id);

        if ($post->is_approved == FALSE) {
            $post->is_approved = TRUE;
            $post->save();

            $post->user->notify(new AuthorPostApproved($post));

            $subscribers = Subscriber::all();

            foreach ($subscribers as $subscriber) {
                Notification::route('mail', $subscriber->email)
                            ->notify(new NewPostNotify($post));
            }

            return redirect(route('admin.post.pending'))->with('successMsg', 'Post approved successfully!');
        } else {
            return redirect(route('admin.post.pending'))->with('successMsg', 'Post is already approved!');
        }

    }
}
