<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Subscriber;
use Illuminate\Http\Request;
use function compact;
use function redirect;
use function route;
use function view;

class SubscriberController extends Controller
{
    public function index()
    {

        $subscribers = Subscriber::latest()->get();

        return view('admin.subscriber.index', compact('subscribers'));

    }

    public function destroy(Subscriber $subscriber)
    {
        $subscriber->delete();

        return redirect(route('admin.subscriber.index'))->with('successMsg', 'Subscriber ' . $subscriber->email . ' deleted successfully!');
    }
}
