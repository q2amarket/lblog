<?php

namespace App\Http\Controllers\Author;

use App\Category;
use App\Http\Controllers\Controller;
use App\Notifications\NewAuthorPost;
use App\Post;
use App\Tag;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use function abort;
use function compact;
use function redirect;
use function route;
use function str_slug;
use function uniqid;
use function view;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $posts = Auth::User()->posts()->latest()->get();

        return view('author.post.index', compact('posts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        $categories = Category::all();
        $tags       = Tag::all();

        return view('author.post.create', compact('categories', 'tags'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title'      => 'required',
            'image'      => 'required|mimes:jpeg,png,jpg',
            'categories' => 'required',
            'tags'       => 'required',
            'body'       => 'required',
        ]);

        // Get form image
        $image = $request->file('image');
        $slug  = str_slug($request->title);

        if (isset($image)) {

            // make unique name
            $currentDate = Carbon::now()->toDateString();
            $imageName   = $slug . '-' . $currentDate . '-' . uniqid() . '.' . $image->getClientOriginalExtension();

            // check if directory is exists
            if ( ! Storage::disk('public')->exists('post')) {
                Storage::disk('public')->makeDirectory('post');
            }

            // resize image for category
            $postImage = Image::make($image)->fit(1600, 1066)->stream();
            Storage::disk('public')->put('post/' . $imageName, $postImage);

        } else {

            $imageName = 'default.jpg';

        }

        $post          = new Post();
        $post->user_id = Auth::id();
        $post->title   = $request->title;
        $post->slug    = $slug;
        $post->image   = $imageName;
        $post->body    = $request->body;
        if (isset($request->status)) {
            $post->status = TRUE;
        } else {
            $post->status = FALSE;
        }
        $post->is_approved = FALSE;
        $post->save();

        $post->categories()->attach($request->categories);
        $post->tags()->attach($request->tags);

        $users = User::where('role_id', 1)->get();
        Notification::send($users, new NewAuthorPost($post));

        return redirect(route('author.post.index'))->with('successMsg', 'Post created successfully! It will be published once approved by admin.');
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Post $post
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(Post $post)
    {
        if ($post->user_id != Auth::id()) {
            return abort(404);
        }

        return view('author.post.show', compact('post'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Post $post
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Post $post)
    {
        if ($post->user_id != Auth::id()) {
            return abort(404);
        }

        $categories = Category::all();
        $tags       = Tag::all();

        return view('author.post.edit', compact('post', 'categories', 'tags'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Post                $post
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, Post $post)
    {
        if ($post->user_id != Auth::id()) {
            return abort(404);
        }

        $this->validate($request, [
            'title'      => 'required',
            'image'      => 'mimes:jpeg,png,jpg',
            'categories' => 'required',
            'tags'       => 'required',
            'body'       => 'required',
        ]);

        // Get form image
        $image = $request->file('image');
        $slug  = str_slug($request->title);

        if (isset($image)) {

            // make unique name
            $currentDate = Carbon::now()->toDateString();
            $imageName   = $slug . '-' . $currentDate . '-' . uniqid() . '.' . $image->getClientOriginalExtension();

            // delete old image
            if (Storage::disk('public')->exists('post/' . $post->image)) {
                Storage::disk('public')->delete('post/' . $post->image);
            }

            // check if directory is exists
            if ( ! Storage::disk('public')->exists('post')) {
                Storage::disk('public')->makeDirectory('post');
            }

            // resize image for category
            $postImage = Image::make($image)->fit(1600, 1066)->stream();
            Storage::disk('public')->put('post/' . $imageName, $postImage);

        } else {

            $imageName = $post->image;

        }

        $post->user_id = Auth::id();
        $post->title   = $request->title;
        $post->slug    = $slug;
        $post->image   = $imageName;
        $post->body    = $request->body;
        if (isset($request->status)) {
            $post->status = TRUE;
        } else {
            $post->status = FALSE;
        }

        if (isset($request->status)) {
            $post->is_approved = TRUE;
        } else {
            $post->is_approved = FALSE;
        }
        $post->save();

        $post->categories()->sync($request->categories);
        $post->tags()->sync($request->tags);

        return redirect(route('author.post.index'))->with('successMsg', 'Post updated successfully!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Post $post
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Exception
     */
    public function destroy(Post $post)
    {
        if ($post->user_id != Auth::id()) {
            return abort(404);
        }

        // delete old image
        if (Storage::disk('public')->exists('post/' . $post->image)) {
            Storage::disk('public')->delete('post/' . $post->image);
        }

        $post->categories()->detach();
        $post->tags()->detach();
        $post->delete();

        return redirect(route('author.post.index'))->with('successMsg', 'Post deleteds successfully!');
    }
}
