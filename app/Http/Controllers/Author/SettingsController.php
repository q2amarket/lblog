<?php

namespace App\Http\Controllers\Author;

use App\Http\Controllers\Controller;
use App\User;
use Auth;
use Carbon\Carbon;
use Hash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use function redirect;
use function str_slug;
use function uniqid;
use function view;

class SettingsController extends Controller
{
    public function index()
    {

        return view('author.settings');
    }

    public function updateProfile(Request $request)
    {

        $this->validate($request, [
            'name'  => 'required',
            'email' => 'required|email',
            'image' => 'image',
        ]);

        // Get form image
        $image = $request->file('image');
        $slug  = str_slug($request->name);
        $user  = User::findOrFail(Auth::id());

        if (isset($image)) {

            // delete old image from user dir
            if (Storage::disk('public')->exists('profile/' . $user->image)) {
                Storage::disk('public')->delete('profile/' . $user->image);
            }

            // make unique name
            $currentdate = Carbon::now()->toDateString();
            $imageName   = $slug . '-' . $currentdate . '-' . uniqid() . '.' . $image->getClientOriginalExtension();

            // check if directory is exists
            if ( ! Storage::disk('public')->exists('profile')) {
                Storage::disk('public')->makeDirectory('profile');
            }

            // resize image for user
            $userImage = Image::make($image)->fit(500, 500)->stream();
            Storage::disk('public')->put('profile/' . $imageName, $userImage);

        } else {

            $imageName = $user->image;

        }

        $user->name  = $request->name;
        $user->email = $request->email;
        $user->image = $imageName;
        $user->about = $request->about;
        $user->save();

        return redirect()->back()->with('successMsg', 'Profile is updated successfully!');

    }

    public function updatePassword(Request $request)
    {

        $this->validate($request, [
            'old_password' => 'required',
            'password'     => 'required|confirmed',
        ]);

        $hasPassword = Auth::user()->password;

        if (Hash::check($request->old_password, $hasPassword)) {

            if ( ! Hash::check($request->password, $hasPassword)) {

                $user           = User::findOrFail(Auth::id());
                $user->password = Hash::make($request->password);
                $user->save();

                Auth::logout();

                return redirect()->back();

            } else {
                return redirect()->back()->withErrors('New password cannot be same as old password.');
            }

        } else {
            return redirect()->back()->withErrors('Incorrect current password.');
        }

    }
}
