<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'role_id' => 1,
            'username' => 'admin',
            'name' => 'Jatin',
            'password' => bcrypt('demo'),
            'email' => 'admin@lblog.com',
        ]);

        DB::table('users')->insert([
            'role_id' => 2,
            'username' => 'author',
            'name' => 'Rahul',
            'password' => bcrypt('demo'),
            'email' => 'author@lblog.com',
        ]);
    }
}
