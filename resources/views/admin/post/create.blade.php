@extends('layouts.backend.app')
@section('title', 'Create Post')

@push('css')
    <!-- Bootstrap Select Css -->
    <link href="{{asset('public/assets/backend/plugins/bootstrap-select/css/bootstrap-select.css')}}" rel="stylesheet"/>
@endpush

@section('content')

    <div class="container-fluid">

        @if($errors->any())
            @foreach($errors->all() as $error)
                <div class="row">
                    <div class="col-xs-12">
                        <div class="alert alert-danger" role="alert">
                            {{$error}}
                        </div>
                    </div>
                </div>
            @endforeach
        @endif

        <form action="{{route('admin.post.store')}}" method="POST" enctype="multipart/form-data">
            @csrf

            <div class="row clearfix">
                <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>ADD NEW Post</h2>
                        </div>

                        <div class="body">
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input type="text" id="title" name="title" class="form-control">
                                    <label for="title" class="form-label">Post Title</label>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="">
                                    <label for="tinymce" class="form-label">Post Content</label>
                                    <textarea name="body" id="tinymce" cols="30" rows="10" class="form-control"
                                              placeholder="Post Content"></textarea>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>Additional Data</h2>
                        </div>

                        <div class="body">

                            <div class="form-group">
                                <div class="form-inline {{$errors->has('categories') ? 'focused error' : ''}}">
                                    <label for="category" class="form-label">Select Categories</label>
                                    <select name="categories[]" id="category" class="form-control show-tick"
                                            data-live-search="true" multiple>
                                        @foreach($categories as $category)
                                            <option value="{{$category->id}}">{{$category->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="form-inline {{$errors->has('tags') ? 'focused error' : ''}}">
                                    <label for="tag" class="form-label">Select Tags</label>
                                    <select name="tags[]" id="tag" class="form-control show-tick"
                                            data-live-search="true" multiple>
                                        @foreach($tags as $tag)
                                            <option value="{{$tag->id}}">{{$tag->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group form-float">
                                <label for="image">Featured Image</label>
                                <input type="file" id="image" name="image" class="form-control">
                            </div>

                            {{--                            <div class="form-group form-float">
                                                            <input type="checkbox" name="status" id="publish" class="filled-in" value="1">
                                                            <label for="publish">Publish</label>
                                                        </div>--}}

                            <div class="demo-switch">
                                <div class="switch">
                                    <label for="publish">Un<input type="checkbox" name="status" id="publish"
                                                                  value="1"><span class="lever"></span>Publish</label>
                                </div>
                            </div>

                            <a href="{{route('admin.post.index')}}"
                               class="btn btn-danger m-t-15 waves-effect">BACK</a>
                            <button type="submit" class="btn btn-primary m-t-15 waves-effect">SUBMIT</button>

                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>

@endsection

@push('js')
    <!-- Select Plugin Js -->
    <script src="{{asset('public/assets/backend/plugins/bootstrap-select/js/bootstrap-select.js')}}"></script>
    <!-- TinyMCE -->
    <script src="{{asset('public/assets/backend/plugins/tinymce/tinymce.js')}}"></script>

    <script type="text/javascript">
        $(function () {
            //TinyMCE
            tinymce.init({
                selector: "textarea#tinymce",
                theme: "modern",
                height: 300,
                plugins: [
                    'advlist autolink lists link image charmap print preview hr anchor pagebreak',
                    'searchreplace wordcount visualblocks visualchars code fullscreen',
                    'insertdatetime media nonbreaking save table contextmenu directionality',
                    'emoticons template paste textcolor colorpicker textpattern imagetools'
                ],
                toolbar1: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
                toolbar2: 'print preview media | forecolor backcolor emoticons',
                image_advtab: true
            });
            tinymce.suffix = ".min";
            tinyMCE.baseURL = '{{asset('public/assets/backend/plugins/tinymce')}}';
        });
    </script>
@endpush
