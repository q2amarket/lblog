@extends('layouts.backend.app')
@section('title', 'Show Post')

@push('css')
@endpush

@section('content')

    <div class="container-fluid">

        <div class="block-header row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <a href="{{route('admin.post.index')}}" class="btn btn-danger waves-effect">BACK</a>

                @if(!$post->is_approved)
                    <button class="btn btn-success waves-effect pull-right" onclick="approvaePost({{$post->id}})" >
                        <i class="material-icons">done</i><span>APPROVE</span></button>

                    <form action="{{route('admin.post.approve', $post->id)}}" method="POST" id="approval-form" style="display: none">
                        @csrf
                        @method('PUT')
                    </form>
                @else
                    <button class="btn btn-default waves-effect pull-right" disabled="disabled">
                        <i class="material-icons">done_all</i><span>APPROVED</span></button>
                @endif
            </div>
        </div>

        <div class="row clearfix">
            <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>
                            {{$post->title}}
                            <small>Posted by: <strong><a href="">{{$post->user->name}}</a></strong>
                                on {{$post->created_at->toFormattedDateString()}}</small>
                        </h2>
                    </div>

                    <div class="body">
                        {!!$post->body!!}
                    </div>
                </div>
            </div>

            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>Categories</h2>
                    </div>

                    <div class="body">
                        @foreach($post->categories as $category)
                            <span class="label bg-cyan">{{$category->name}}</span>
                        @endforeach

                    </div>
                </div>

                <div class="card">
                    <div class="header">
                        <h2>Tags</h2>
                    </div>

                    <div class="body">
                        @foreach($post->tags as $tag)
                            <span class="label bg-cyan">{{$tag->name}}</span>
                        @endforeach
                    </div>
                </div>

                <div class="card">
                    <div class="header">
                        <h2>Featured Image</h2>
                    </div>

                    <div class="body">
                        <img class="img-responsive" src="{{asset('public/storage/post/'.$post->image)}}"
                             alt="featured-image-{{$post->id}}">
                    </div>
                </div>
            </div>
        </div>

    </div>

@endsection

@push('js')
    <!-- Select Plugin Js -->
    <script src="{{asset('public/assets/backend/plugins/bootstrap-select/js/bootstrap-select.js')}}"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
    <script>
        function approvaePost($id) {
            const swalWithBootstrapButtons = Swal.mixin({
                customClass: {
                    confirmButton: 'btn btn-success',
                    cancelButton: 'btn btn-danger'
                },
                buttonsStyling: false
            })

            swalWithBootstrapButtons.fire({
                title: 'Are you sure?',
                text: "You want to approve this post?",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Yes, approve it!',
                cancelButtonText: 'No, cancel!',
                reverseButtons: true
            }).then((result) => {
                if (result.value) {

                    event.preventDefault();
                    document.getElementById('approval-form').submit();

                } else if (
                    /* Read more about handling dismissals below */
                    result.dismiss === Swal.DismissReason.cancel
                ) {
                    swalWithBootstrapButtons.fire(
                        'Cancelled',
                        'The post remains pending :)',
                        'info'
                    )
                }
            })
        }
    </script>

@endpush
