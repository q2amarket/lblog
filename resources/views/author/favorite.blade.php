@extends('layouts.backend.app')
@section('title', 'Favorite Posts')

@push('css')
    <link href="{{asset('public/assets/backend/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css')}}"
          rel="stylesheet">
@endpush

@section('content')

    <div class="container-fluid">
        <div class="block-header row clearfix">
            <div class="col-xs-6 col-sm-6 col-md-9 col-lg-10">
                <h2 class="align-left d-inline">FAVORITE POSTS</h2>
            </div>
        </div>

    <!-- Exportable Table -->
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>ALL FAVORITE POSTS <span class="badge bg-info">{{$posts->count()}}</span></h2>
                    </div>
                    <div class="body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Title</th>
                                    <th>Author</th>
                                    <th class="text-center"><i class="material-icons">favorite</i></th>
                                    <th class="text-center"><i class="material-icons">comment</i></th>
                                    <th class="text-center"><i class="material-icons">visibility</i></th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tfoot>
                                <tr>
                                    <th>ID</th>
                                    <th>Title</th>
                                    <th>Author</th>
                                    <th class="text-center"><i class="material-icons">favorite</i></th>
                                    <th class="text-center"><i class="material-icons">comment</i></th>
                                    <th class="text-center"><i class="material-icons">visibility</i></th>
                                    <th>Action</th>
                                </tr>
                                </tfoot>
                                <tbody>
                                @foreach($posts as $key => $post)
                                    <tr>
                                        <td>{{$key + 1}}</td>
                                        <td>{{str_limit($post->title, 75)}}</td>
                                        <td>{{$post->user->name}}</td>
                                        <td class="text-center">{{$post->favorite_to_users->count()}}</td>
                                        <td class="text-center">0</td>
                                        <td class="text-center">{{$post->view_count}}</td>

                                        <td>{{$post->updated_at->toFormattedDateString()}}</td>
                                        <td class="text-center">
                                            <a href="{{route('admin.post.show', $post->id)}}"
                                               class="btn btn-primary btn-sm waves-effect"><i
                                                    class="material-icons">visibility</i></a>

                                            <button class="btn btn-danger btn-sm waves-effect"
                                                    onclick="removeePost({{$post->id}})">
                                                <i class="material-icons">delete</i></button>
                                            <form action="{{route('post.favorite', $post->id)}}"
                                                  id="remove-form-{{$post->id}}" method="POST"
                                                  style="display: none">
                                                @csrf
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- #END# Exportable Table -->

    </div>


@endsection

@push('js')

    <!-- Jquery DataTable Plugin Js -->
    <script src="{{asset('public/assets/backend/plugins/jquery-datatable/jquery.dataTables.js')}}"></script>
    <script
        src="{{asset('public/assets/backend/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js')}}"></script>
    <script
        src="{{asset('public/assets/backend/plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js')}}"></script>
    <script
        src="{{asset('public/assets/backend/plugins/jquery-datatable/extensions/export/buttons.flash.min.js')}}"></script>
    <script src="{{asset('public/assets/backend/plugins/jquery-datatable/extensions/export/jszip.min.js')}}"></script>
    <script src="{{asset('public/assets/backend/plugins/jquery-datatable/extensions/export/pdfmake.min.js')}}"></script>
    <script src="{{asset('public/assets/backend/plugins/jquery-datatable/extensions/export/vfs_fonts.js')}}"></script>
    <script
        src="{{asset('public/assets/backend/plugins/jquery-datatable/extensions/export/buttons.html5.min.js')}}"></script>
    <script
        src="{{asset('public/assets/backend/plugins/jquery-datatable/extensions/export/buttons.print.min.js')}}"></script>

    <!-- Custom Js -->
    <script src="{{asset('public/assets/backend/js/pages/tables/jquery-datatable.js')}}"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
    <script>
        function removePost($id) {
            const swalWithBootstrapButtons = Swal.mixin({
                customClass: {
                    confirmButton: 'btn btn-success',
                    cancelButton: 'btn btn-danger'
                },
                buttonsStyling: false
            })

            swalWithBootstrapButtons.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Yes, delete it!',
                cancelButtonText: 'No, cancel!',
                reverseButtons: true
            }).then((result) => {
                if (result.value) {

                    event.preventDefault();
                    document.getElementById('remove-form-' + $id).submit();

                } else if (
                    /* Read more about handling dismissals below */
                    result.dismiss === Swal.DismissReason.cancel
                ) {
                    swalWithBootstrapButtons.fire(
                        'Cancelled',
                        'Your imaginary file is safe :)',
                        'error'
                    )
                }
            })
        }
    </script>

@endpush
