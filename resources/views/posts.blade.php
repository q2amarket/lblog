@extends('layouts.frontend.app')
@section('title', 'All Posts')

@push('css')
    <!-- Welcome/Home Page CSS -->
    <link href="{{asset('public/assets/frontend/css/category/styles.css')}}" rel="stylesheet">
    <link href="{{asset('public/assets/frontend/css/category/responsive.css')}}" rel="stylesheet">
    <style>
        .favorite {
            color: deeppink;
        }
    </style>
@endpush

@section('content')

    <div class="slider display-table center-text">
        <h1 class="title display-table-cell"><b>ALL POSTS</b></h1>
    </div><!-- slider -->

    <section class="blog-area section">
        <div class="container">

            <div class="row">

                @foreach($posts as $post)
                    <div class="col-lg-4 col-md-6">
                        <div class="card h-100">
                            <div class="single-post post-style-1">

                                <div class="blog-image"><img src="{{asset('public/storage/post/' . $post->image)}}"
                                                             alt="Blog Image">
                                </div>

                                <a class="avatar" href="#"><img
                                        src="{{asset('public/storage/profile/' . $post->user->image)}}"
                                        alt="Profile Image"></a>

                                <div class="blog-info">

                                    <h4 class="title"><a href="{{route('post.details', $post->slug)}}"><b>{{str_limit($post->title, 50, '...')}}</b></a></h4>
                                    <ul class="post-footer">
                                        <li>
                                            @guest()
                                                <a href="#" onclick="fav()"><i
                                                        class="ion-heart"></i>{{$post->favorite_to_users->count()}}</a>
                                            @else
                                                <a href="javascript::void(0)"
                                                   onclick="document.getElementById('favorite-form-{{$post->id}}').submit();"
                                                   class="{{Auth::user()->favorite_posts()->where('post_id', $post->id)->count() != 0 ? 'favorite' : ''}}"

                                                ><i
                                                        class="ion-heart"></i>{{$post->favorite_to_users->count()}}</a>
                                                <form id="favorite-form-{{$post->id}}"
                                                      action="{{route('post.favorite', $post->id)}}" method="POST"
                                                      style="display: none">
                                                    @csrf
                                                </form>
                                            @endguest
                                        </li>
                                        <li><a href="#"><i class="ion-chatbubble"></i>6</a></li>
                                        <li><a href="#"><i class="ion-eye"></i>{{$post->view_count}}</a></li>
                                    </ul>

                                </div><!-- blog-info -->
                            </div><!-- single-post -->
                        </div><!-- card -->
                    </div><!-- col-lg-4 col-md-6 -->
                @endforeach

            </div><!-- row -->

            {{$posts->links()}}

        </div><!-- container -->
    </section><!-- section -->

@endsection

@push('js')
@endpush
